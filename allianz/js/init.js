/*****************************************************************************
 *    Proyecto: Allianz
 *    Desripcion: Inicialización de componentes
 ******************************************************************************/
/*NOTA A PROYECTO: Evitar modificar en la medida de lo posible, las extensiones
 * de funcionalidad realizarlas siempre que sea posible en main.js*/

var dFrontJs = (function () {

    var maxLarge = 1600,
        maxMedium = 1280,
        maxSmall = 768,
        maxXSmall = 420;
        isInitMachine = false;
        c = 0;

    var initHideCookies = (function (selector) {
        //Hide Cookies
        $(selector).click(function () {
            $(".mod_cookie").hide();
        });
    })

    var initCombobox = (function (selector, params) {
        var item = $(selector);
        var settings = params || {appendTo: ".dFront"};

        item.selectmenu(settings);
    });

    var initHeaderSearch = (function (selector, hideItem) {
        var items = $(".mod_navMenu .nvm-listItem");
        var children = $(hideItem) || $(selector);
        $(selector).click(function (e) {
            if ($(window).innerWidth() > 1280 - 22) {
                $(this).parents(".hdr-search").toggleClass("active");
                for (var i = 0, itemsLength = items.length; i < itemsLength; i++) {
                    var current = $(items[i]);
                    if (current.hasClass("hdr-zone") !== true && current.hasClass("hdr-search") !== true) {
                        current.toggleClass("hide");
                    }
                }
                e.stopPropagation();
            }
        });
        children.click(function (e) {
            e.stopPropagation();
        });
    });

    var initMobileMenu = (function (selector) {
        var overlay = $('<div class="nvm-overlay"></div>');
        var item = $(selector);
        var menu = item.next();
        item.on("click dblclick", function (e) {
            if (!item.next().is(":visible") && item.is(":visible")) {
                item.next().toggle("blind");
                item.toggleClass("active");
                overlay.appendTo(".mod_navMenu");
                e.stopPropagation();
            }
            e.preventDefault();
        });
        menu.click(function (e) {
            e.stopPropagation();
        });
        $("html").click(function () {
            if (item.next().is(":visible") && item.is(":visible")) {
                overlay.remove();
                item.next().toggle("blind");
                item.toggleClass("active");
            }
        });
    });

    var initCarruselFade = (function (selector, params) {
        var item = $(selector);
        var settings = params || {
                dots: true,
                infinite: true,
                speed: 500,
                fade: true,
                cssEase: 'linear',
                arrows: false
            };

        item.slick(settings);

    });

    /*    var initCarruselCenter = (function (selector, params) {
     var item = $(selector);

     item.slick({
     centerMode: true,
     centerPadding: '60px',
     slidesToShow: 3,
     variableWidth: true,
     responsive: [
     {
     breakpoint: 768,
     settings: {
     arrows: false,
     centerMode: true,
     centerPadding: '40px',
     slidesToShow: 3
     }
     },
     {
     breakpoint: 480,
     settings: {
     arrows: false,
     centerMode: true,
     centerPadding: '40px',
     slidesToShow: 1
     }
     }
     ]
     });
     });*/

    var initCarruselCenter = (function (selector, params) {
        if (!params){
            params == {};
        };
        var item = $(selector),
            contentSwipe = item.find('ul');
        // Desplazamiento lateral en mobile.
        contentSwipe.on({
            swipeleft: function(){
                $(".sdrC-btnP").click();
            },
            swiperight: function(){
                $(".sdrC-btnN").click();
            }
        });

        // Ancho del slide visible
        $(selector).find("li").disableSelection();

        $(".sdrC-content_item").css({
            'width': $(".sdrC-content_center").innerWidth()
        });
        // ancho del slide visible en el resize.
        $(window).on("resize",function(){
            var widthItem = $(".sdrC-content_center").innerWidth();
            $(".sdrC-content_item").css({
                'width': widthItem
            });
        });
        function changePositionCarrusel(params){
            // Funcion para hacer infinito el carrusel
            var _infinity = function(box, item, value){

                if(value){
                    //console.log("entra en uno");
                    box.addClass("sdrC-content_center");
                }else{
                    //console.log("entra en cero");
                    box.removeClass("sdrC-content_center");
                }
                //item.removeAttr("style");
            }
            // funcion para controlar las animaciones del carrusel
            var _animateItem = function (item, ancho, opacity, timeDur) {
                item.animate({
                    'width': ancho + "%",
                    'opacity': opacity,
                    "overflow": "visible"
                }, timeDur, function(){
                    _infinity($(this), params.itemListado, parseInt(opacity))
                });
            }
            //console.log("entra en la funcion");
            if(params.element.hasClass('sdrC-btnP')){
                // console.log('anterior');
                //desplazamos a la izquierda el primer item

                params.element.attr("disabled", "disabled");
                $(params.primerItem).animate({
                    'marginLeft': params.anchoItem * (-1) + "%"
                }, 500, function () {
                    // Una vez completada la animacion hacemos el carrusel infinito
                    params.itemListado.removeAttr("style");
                    params.primerItem.clone().appendTo(params.listado);
                    params.primerItem.remove();
                    $(".sdrC-btnP").removeAttr("disabled");
                });
                //transformamos el item central en uno normal
                _animateItem(params.itemNext, params.anchoItemCentro,1,500);
                _animateItem(params.itemCentro, params.anchoItem, 0.2,500);

                if($(window).innerWidth() < 768){
                    _animateItem(params.itemNext.next(), 18,0.2,500);
                }

            } else if (params.element.hasClass('sdrC-btnN')){
                // console.log('');
                //desplazamos a la derecha el ultimo item
                params.primerItem.css('visibility', 'visible');
                params.primerItem.css('overflow', 'visible');
                params.ultimoItem.clone().prependTo(params.listado).css('marginLeft', '-10%').addClass("itemFirst");
                params.element.attr("disabled", "disabled");

                $(".itemFirst").animate({
                    'marginLeft': '0%',
                    "overflow": "visible"
                }, 500, function () {
                    // Una vez completada la animacion hacemos el carrusel infinito
                    //console.log('Esto no se ejecuta')
                    $(this).removeClass("itemFirst");
                    params.itemListado.removeAttr("style");
                    params.ultimoItem.remove();
                    $(".sdrC-btnN").removeAttr("disabled");
                });
                //transformamos el item central en uno normal

                _animateItem(params.itemPrev, params.anchoItemCentro,1,500);

                if($(window).innerWidth() < 768){
                    _animateItem(params.itemCentro, 18, 0.2,500);
                    _animateItem(params.itemCentro.next(), 0,0.2,500);
                }else{
                    _animateItem(params.itemCentro, params.anchoItem, 0.2,500);
                }
            };
        };

        // Ejecucion de los clicks
        $('.sdrC-btnP, .sdrC-btnN').on('click', function(){

            var anchoItem = (10 * 100) / 110;
            var anchoItemCentro = (70 * 100) / 110;
            var anchoDoc = 0;

            if($(window).innerWidth() < 768){
                anchoItemCentro = 82;
                anchoItem = 0;
            }
            // params = Parametros del botton al que hacemos click
            var params = {
                'element' : $(this),
                'itemCentro' : $(".sdrC-content_center"),
                'itemNext' : $(".sdrC-content_center").next(),
                'itemPrev' : $(".sdrC-content_center").prev(),
                'widthItem' : $(".sdrC-content_center").innerWidth(),
                'listado' : $(this).parent().find("ul"),
                'itemListado' : $(this).parent().find("ul > li"),
                'primerItem' : $(this).parent().find("ul > li:first-child"),
                'ultimoItem' : $(this).parent().find("ul > li:last-child"),
                'anchoItem' : anchoItem,
                'anchoItemCentro' : anchoItemCentro
            };
            //console.log(params);
            changePositionCarrusel(params);
        });
    });

    // Carrusel de productos del pie de pagina
    var carruselProductos = (function (selector, params) {
        var content = $(selector);
        var paramsSlide = params || {
                infinite: false,
                slidesToShow: 6,
                slidesToScroll: 1,
                speed: 500,
                arrows: true,
                responsive: [
                    {
                        breakpoint: 1280,
                        settings: {
                            infinite: true,
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            arrows: false,
                            centerMode: true,
                            centerPadding: "150px"
                        }
                    },
                    {
                        breakpoint: 1120,
                        settings: {
                            infinite: true,
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            arrows: false,
                            centerMode: true,
                            centerPadding: "130px"
                        }
                    },
                    {
                        breakpoint: 1024,
                        settings: {
                            infinite: true,
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            arrows: false,
                            centerMode: true,
                            centerPadding: "120px"
                        }
                    },
                    {
                        breakpoint: 920,
                        settings: {
                            infinite: true,
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            arrows: false,
                            centerMode: true,
                            centerPadding: "100px"
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            infinite: true,
                            slidesToShow: 1,
                            arrows: false,
                            centerMode: true,
                            centerPadding: "170px"
                        }
                    },
                    {
                        breakpoint: 690,
                        settings: {
                            infinite: true,
                            slidesToShow: 1,
                            arrows: false,
                            centerMode: true,
                            centerPadding: '150px'
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            infinite: true,
                            slidesToShow: 1,
                            arrows: false,
                            centerMode: true,
                            centerPadding: '130px'
                        }
                    },
                    {
                        breakpoint: 520,
                        settings: {
                            infinite: true,
                            slidesToShow: 1,
                            arrows: false,
                            centerMode: true,
                            centerPadding: '110px'
                        }
                    },
                    {
                        breakpoint: 430,
                        settings: {
                            infinite: true,
                            slidesToShow: 1,
                            arrows: false,
                            centerMode: true,
                            centerPadding: '90px'
                        }
                    },
                    {
                        breakpoint: 380,
                        settings: {
                            infinite: true,
                            slidesToShow: 1,
                            arrows: false,
                            centerMode: true,
                            centerPadding: '70px'
                        }
                    }
                ]
            };
        content.slick(paramsSlide);
    });

    var comprobarScroll = (function () {
        if ($(window).height() < $(document).height()) {
            $('.fc_slkProduts').css({
                position: "absolute",
                left: "-999em",
                top: 0
            });
            scrollToBotton();
        }
    });

    var scrollToBotton = (function () {
        var div = $(".fc_slkProduts");
        if ($(window).scrollTop() + $(window).innerHeight() >= $(document).innerHeight() - 5) {
            if (!div.hasClass("active")) {
                div.addClass("active");
                setTimeout(function () {
                    var height = div.height();
                    var scrollY = $(document).innerHeight();
                    div.css({
                        position: "",
                        left: "",
                        top: "",
                        overflow: "hidden",
                        marginTop: height,
                        height: 0
                    });
                    // SCROLL TOP
                    // $('html, body').animate({
                    //     scrollTop: scrollY + div.innerHeight()
                    // }, 1000);
                    div.animate({
                        marginTop: 0,
                        height: height
                    }, 1000, function () {
                        $(this).css({
                            display: "",
                            overflow: "",
                            height: "",
                            marginTop: ""
                        });
                    });
                }, 500);
            }
        }
        $(window).on("scroll", function () {
            if ($(window).scrollTop() + $(window).innerHeight() >= $(document).innerHeight() - 5) {
                if (!div.hasClass("active")) {
                    div.addClass("active");
                    setTimeout(function () {
                        var height = div.height();
                        var scrollY = $(document).innerHeight();
                        div.css({
                            position: "",
                            left: "",
                            top: "",
                            overflow: "hidden",
                            marginTop: height,
                            height: 0
                        });
                        // SCROLL TO 
                        // $('html, body').animate({
                        //     scrollTop: scrollY + div.innerHeight()
                        // }, 1000);
                        div.animate({
                            marginTop: 0,
                            height: height
                        }, 1000, function () {
                            $(this).css({
                                display: "",
                                overflow: "",
                                height: "",
                                marginTop: ""
                            });
                        });
                    }, 500);
                }
            } else if ($(window).scrollTop() + $(window).innerHeight() < $(document).height() - $("footer").innerHeight()) {
                if (div.hasClass("active")) {
                    div.css({
                        position: "absolute",
                        left: "-999em",
                        top: 0
                    }).removeClass("active");
                }
            }
        });
    });

    var colocarBtnSliderFade = (function (selector) {
        var item = $(selector);

        if (item.length > 0) {

            var anchoPantalla = $(window).width();
            var restoAnchoPantalla = anchoPantalla - 1600;

            if (restoAnchoPantalla < 0) {
                restoAnchoPantalla = 0;
            }

            var positionParrafo = item.offset();
            $(".slick-dots").css("left", (positionParrafo.left - (restoAnchoPantalla / 2)) + "px");

        }
    });

    var colocarBtnSliderCenter = (function (selector) {
        var item = $(selector);

        if (item.length > 0) {

            //colocamos los botones del carrusel a los lados del item priencipal
            var itemCentro = $(".sdrC-content_center");
            var btnP = $(".sdrC-btnP");
            var btnN = $(".sdrC-btnN");
            var anchoItemCentro = itemCentro.width();
            var anchoBtnP = btnP.width();
            var anchoBtnN = btnN.width();
            var position = itemCentro.offset();
            var anchoPantalla = $(window).width();
            var restoAnchoPantalla = anchoPantalla - 1600;

            if (restoAnchoPantalla < 0) {
                restoAnchoPantalla = 0;
            }

            btnP.css("left", (position.left - anchoBtnP - 15 - (restoAnchoPantalla / 2)) + "px");
            btnN.css("left", (position.left + anchoItemCentro + 15 - (restoAnchoPantalla / 2)) + "px");
        }
    });

    var initAccordion = (function (selector, params, headerSameHeight) {
        if ((typeof $.fn.accordion).toLowerCase() == "function") {
            var items = $(selector);
            var paramsAccordion = params || {
                collapsible: true,
                heightStyle: "content"
            };

            if ((typeof headerSameHeight).toLowerCase() === "string") {
                var height = 0;
                for (var cont = 0, leng = items.length; cont < leng; cont++) {
                    var current = $(items[cont]);
                    var h = current.find(headerSameHeight).outerHeight();
                    if (h > height) {
                        height = h;
                    }
                }
                items.find(headerSameHeight).css("min-height", height);
            }
            items.accordion(paramsAccordion);
        }
    });

    // http://ghusse.github.io/jQRangeSlider/
    var initRangeSlider = (function(selector,params){
        var paramsMobile = params || {
            bounds: {min: 18, max: 70},
            arrows:false,
            defaultValues:{min: 35, max: 50},
            wheelMode: "zoom"
            //enabled: false                        //disabled slider
            //range: {min: 10, max: 40}             //max range
            //step: 10                              //step
        };
        var filter = $(selector).parents(".flt-box");
        if (filter.is(":hidden")){
            filter.show();
            $(selector).rangeSlider(paramsMobile);
            setTimeout(function(){
                filter.hide();
            },100);
        }
        else{
            $(selector).rangeSlider(paramsMobile);
        }
    });

    var initFilter = (function(selector){
        var filter = $(selector).parents(".mod_filter").find(".flt-box");
        $(selector).click(function(e){
            $(".fc-filter_showHide").toggle();
            $(".mod_title").toggle();
            $(".mod_machine").toggle();
            $(".mod_tags").toggle();
            $(".mod_imageList").toggle();
            $(".mod_footer").toggle();
            filter.toggle("blind");
            $(this).parent().toggleClass("active");
            e.preventDefault();
        });
    });

    //var initDatepickerView = (function(selector){
    //
    //    $(selector).datepicker({
    //      monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
    //      monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
    //      dayNames: [ "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado" ],
    //      dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
    //      firstDay: 1
    //    });
    //
    //});

// Contenido linkSlide show / hide
    var contentSmsInstant = (function (link, content){
        var item = $(link),
            content = $(content);

        item.on('click', function(event){
            event.preventDefault();
            item.toggleClass('xui-activo');
            content.slideToggle();
        });

    });

    // Initialitation datepicker component ()
    var _onDatepickerShow = function(datepicker) {
        var datepickerContainer = datepicker.div;
        $(datepickerContainer).position({
            my: "right top",
            at: "right bottom+10",
            of: $("#" + datepicker.id)
        });
    };
    var initDatepicker = (function(selector, params, opts) {
        if($(selector).length >0){
            datePickerController.setGlobalOptions(opts);
            var elements = $(selector);

            if (!params) {
                params = {};
            }
            if (params.callbackFunctions === undefined) {
                params.callbackFunctions = {};
            }
            params.callbackFunctions.datepickershow = [_onDatepickerShow];
            for (var cont = 0, elementsLength = elements.length; cont < elementsLength; cont++) {
                var current = $(elements[cont]);
                var id = current.attr("id");
                var paramsCurrent = $.extend({}, params);
                paramsCurrent.formElements = {};
                paramsCurrent.formElements[id] = "%d/%m/%Y";
                if (id !== undefined) {
                    datePickerController.createDatePicker(paramsCurrent);
                }
            }
        }
    });
    // End Datepicker

    var _addEventsDatepicker = function (listDate) {
        var date = "20151212";
        alert($(".cd-" + date).attr("class"))
        $(".cd-" + date).addClass("popo")
    };
    var initDatepickerView = (function(selector, params, opts) {
        if($(selector).length >0){
            datePickerController.setGlobalOptions(opts);
            var elements = $(selector);


            if (!params) {
                params = {};
            }
            if (params.callbackFunctions === undefined) {
                params.callbackFunctions = {};
            }
            params.callbackFunctions.datepickershow = [_onDatepickerShow];

            for (var cont = 0, elementsLength = elements.length; cont < elementsLength; cont++) {
                var current = $(elements[cont]);
                var id = current.attr("id");
                var paramsCurrent = $.extend({}, params);
                paramsCurrent.formElements = {};
                paramsCurrent.formElements[id] = "%d/%m/%Y";

                if (id !== undefined) {
                    datePickerController.createDatePicker(paramsCurrent);
                }
            }
        }
        //Una vez creado insertamos los dias con eventos
        //_addEventsDatepicker();

    });
    // End Datepicker
    // Timepicker
    var timePicker = (function (selector, params) {
        if (!params) {
            params = {};
        }
        if ($(selector).length){
            $(selector).timepicker(params);
        }
    });

    // End timePicker

    var initMachineOpen = (function(selector){
        $(selector).on("click",function(){

            $(this).toggleClass("active");
            $(selector).parents(".mod_machine").find(".mch-container").toggle("blind");
            if(!isInitMachine){
                dFrontJs.initMachine('.slot0,.slot1,.slot2');
            }
            isInitMachine=true;


        });

    });
    /*
    // https://github.com/matthewlein/jQuery-jSlots
    */

    var initMachine = (function (selector, params) {
        if (!params) {
            params = {};
        }
        // Objeto que devolvemos del resultado.
        var result = {};
        $(selector).each(function(index, element){
            var node = "."+element.className.substring(5);
            //console.log(node);
            if ($(this).length){
                $(this).jSlots({
                    number : 1,
                    spinner : '#playFancy',
                    easing : 'easeOutSine',
                    time : 7000,
                    loops : 6,
                    onStart : function() {
                        $('.li-conten').removeClass('ico_winner');
                        $('.li-conten').removeClass('stateInit');
                    },
                    onEnd : function(finalNumbers) {
                       // console.log("Entra en onEnd")
                        console.log(finalNumbers);
                        if(finalNumbers == 1){finalNumbers = 2;}
                        //console.log($(node));
                        $(node).find('li').eq(finalNumbers-1).children().addClass('ico_winner');
                        result[node] =  $(node).find('li').eq(finalNumbers-1).children().attr('class');
                        //console.log(result);
                    },
                    onWin: function(winCount, winners){
                        $.each(winners, function() {
                            console.log(this);
                        });
                    }
                });

                $(selector).css({
                    "top": "-100px"
                })
            }
        })
    });
    
    // var machineReset = (function(selector){
    //     var nodo = $(selector);
    //      $
    // });

    var initBtnMachine = function(selector){
        var btnLink = $(selector).find('a');



        var _slidePos = function (iItem, dir){
            var slotSelect = $('.slot'+iItem),
                posTop = parseInt(slotSelect.css('top')),
                slotItems = slotSelect.find('.li-conten'),
                classPos;

            // Eliminamos la clase que pone el icono en naranja y del estado inicial
            slotItems.removeClass('ico_winner');
            slotItems.removeClass('stateInit');

            // Comprobamos si el slot debe de ir en una dirección o en otra.
            if (dir == "up"){
                inc = -100;
                // Comprobamos si estamos en el último item de la direccion de subida
                if (posTop == -500){
                    posTop = -100;
                }else{
                    posTop = posTop+inc;
                }
            }else if(dir == "down"){
                inc = 100;
                // Comprobamos si estamos en el último item de la direccion de bajada
                if (posTop == -100){
                    posTop = -500;
                }else{
                    posTop = posTop+inc;
                }
            }
            // Calculamos la posicion del item que tiene el foco
            classPos = (posTop*(-1)/100);
            //console.log('position: '+classPos)
            // Desplazamos la lista a su nueva posicion
            slotSelect.css('top', ''+posTop+'px');

            // añadimos la clase para activar el icono
            slotItems.eq(classPos).addClass('ico_winner');

            //console.log(posTop);
        };

        btnLink.on('click', function(e){
            e.preventDefault();
            var item = $(this).parent(), // Subo un nivel
                nodoItem = item.parent(),// Subo dos niveles
                indexItem = item.index(); // Number | Position Link

                //console.log(indexItem)
            // Diferenciar entre up y down
            if(nodoItem.hasClass('fc-up')){
                //console.log('botom UP');
               _slidePos(indexItem, "up");
            } else if (nodoItem.hasClass('fc-down')){
                //console.log('botom Down');
                _slidePos(indexItem, "down");
            };
        });
        // Desplazamiento tactil
        $('.slot0').on({
            swipeup: function(){
                 _slidePos(0, "up");
            },
            swipedwon: function(){
                _slidePos(0, "down");
            }
        })
    };

    var initVerMas = function(selector){
        var boxIzq = $('.mv-boxL'),
            boxDcha = $('.mv-boxR'),
            nodos = $('.fc-homeRow'),
            linkMore = $(selector).find('a');
            spnText = $(selector).find('span');
        
        $(selector).click(function(e){
            c++;   
            // console.log(c)
            e.preventDefault();
                if(c == 1){
                    $(nodos[1]).toggle("blind");
                    $(boxDcha[1]).toggle("blind");
                    $(boxIzq[1]).toggle("blind");
                }
                else if(c == 2){
                    $(nodos[2]).toggle("blind");
                    $(boxIzq[2]).toggle("blind");
                    $(boxDcha[2]).toggle("blind");
                    $(linkMore).addClass('active');
                    $(spnText).html('Cerrar');
                }
                else{
                    $('body').animate({
                        scrollTop: 0
                    }, 1000, function(){
                        // console.log('animate finish')
                        $(nodos[1]).toggle("blind");
                        $(nodos[2]).toggle("blind");
                        $(boxIzq[1]).toggle("blind");
                        $(boxIzq[2]).toggle("blind");
                        $(boxDcha[1]).toggle("blind");
                        $(boxDcha[2]).toggle("blind");
                        $(linkMore).removeClass('active');
                        $(spnText).html('Ver más');
                    });
                    c = 0;
                }
        });
    }

    var initDownPageBtn = (function (selector, params) {
        if (!params) {
            params = {};
        }
        if ($(selector).length){
            $(selector).click(function () {

                var altoCookies = 0;
                var altoCabecera = 0;
                var altoHeadPage = 0;
                var paddingHeadPage = 0;
                var extra = 30;

                if(!$(".mod_cookie").is(':hidden')){
                    altoCookies = $(".mod_cookie").height();
                }
                if(!$(".mod_header").is(':hidden')){
                    altoCabecera = $(".mod_header").height();
                }
                if(!$(".hr1-content").is(':hidden')){
                    altoHeadPage = $(".hr1-content").height();
                    paddingHeadPage = parseInt($(".hr1-content").css("paddingTop"));
                }

                $("body,html").animate({scrollTop:(altoCookies + altoCabecera + altoHeadPage + paddingHeadPage + extra)}, '800');
            })
        }
    });

    var controlInputFile = (function (selector, params){
        // Functions
        function _inputValue(e){
            //console.log('Entra inputValue');
            gHide.find('label').click();
            e.stopPropagation();
            e.preventDefault();
        }
        if (!params){
            params = {};
        }
        var nodo = $(selector),
            gShow = nodo.find('.fc-groupShow'),
            gShowInput = gShow.find('input'),
            gShowLabel = gShow.find('label'),
            gHide = nodo.find('.fc-groupHide'),
            gHideInput = gHide.find('input');

        gShowInput.click(function(e) {_inputValue(e)});
        gShowLabel.click(function(e) {_inputValue(e)});

        gHideInput.change(function(){
            gShowInput.val(''+gHideInput.val().replace(/C:\\fakepath\\/i, '')+'');
        });

    });

    var eraserTags = (function(selector){
        var nodo = $(selector);
            itemClose = nodo.find('a');

            itemClose.on('click', function(e){  
                e.preventDefault();
                $(this).parent().parent().remove();
            });
    });


    return {
        initCombobox: initCombobox,
        initMobileMenu: initMobileMenu,
        initHeaderSearch: initHeaderSearch,
        initCarruselFade: initCarruselFade,
        initCarruselCenter: initCarruselCenter,
        colocarBtnSliderFade: colocarBtnSliderFade,
        colocarBtnSliderCenter: colocarBtnSliderCenter,
        initCarruselProducts: carruselProductos,
        initcomprobarScroll: comprobarScroll,
        initHideCookies: initHideCookies,
        initAccordion: initAccordion,
        initRangeSlider: initRangeSlider,
        initFilter: initFilter,
        initMachineOpen: initMachineOpen,
        initDatepickerView: initDatepickerView,
        initCntSmsInstant: contentSmsInstant,
        initDatepicker: initDatepicker,
        initTimePicker: timePicker,
        initDatepickerView: initDatepickerView,
        initMachine: initMachine,
        initVerMas: initVerMas,
        initDownPageBtn: initDownPageBtn,
        initBtnMachine: initBtnMachine,
        initControlInputFile: controlInputFile,
        initEraserTags: eraserTags,
        // initMachineReset: machineReset,
    };

})();

$(document).ready(function () {

    //Inicializaciones
    dFrontJs.initHideCookies(".cok-btn");
    dFrontJs.initCombobox(".fc-combobox_init");
    dFrontJs.initMobileMenu(".fc-menuMobile_init");
    dFrontJs.initCarruselFade(".fc-sliderFade");
    dFrontJs.initCarruselCenter(".fc-sliderCenter");
    dFrontJs.colocarBtnSliderFade(".mod-sliderFade .slf-content .slf-groupParraf");
    dFrontJs.colocarBtnSliderCenter(".mod-sliderCenter");
    dFrontJs.initHeaderSearch(".fc-searchButton_init", ".hdr-searchField");
    dFrontJs.initCarruselProducts('.fc_slkProduts');
    dFrontJs.initcomprobarScroll();
    dFrontJs.initAccordion(".fc-accordion_init", {header: ".fc-accordion_head", collapsible: true, heightStyle: "content"});
    dFrontJs.initRangeSlider(".fc-rangeSlider_init");
    dFrontJs.initFilter(".fc-filter_init");
    dFrontJs.initMachineOpen(".fc-initMachine_open");
    //dFrontJs.initMachine('.slot0,.slot1,.slot2');
    dFrontJs.initBtnMachine('.fc-linkMachine');
    dFrontJs.initDatepickerView(".fmf-datepickerView");
    dFrontJs.initDownPageBtn(".fc-downPage");
    dFrontJs.initCntSmsInstant('.fc-slideLink','.fc-slideContent');
    dFrontJs.initVerMas('.fc-verMas');
    dFrontJs.initCntSmsInstant('.fc-linkMotive','.fc-contentMotive');

    // Inicializacion datepicker
    dFrontJs.initDatepicker(".fc-datepicker_init", {
            // Associate the text input to a DD/MM/YYYY date format
            // Show the week numbers
            showWeeks: false,
            // Set a statusbar format
            //statusFormat: "%d%S %F %Y",
            // Remove the "Today" button
            noTodayButton: true,
            // Fill the entire grid with dates
            fillGrid: false,
            // Enable the selection of dates not within the current month
            // but rendered within the grid (as we used fillGrid:true)
            constrainSelection: false
            //para restringir todos los dias anteriores a hoy por ejemplo: rangeLow:new Date()
            // Set a range low of 13/03/1970 using a YYYYMMDD format String (Example): rangeLow:"19700313"
        },
        {
            //opts Opciones del datepicker
            "nodrag": 1
            //"lang": "en" // "lang":"en-US" (requiere libreria en carpeta lang)
        }
    );

        // Inicializacion datepicker visible
    dFrontJs.initDatepickerView(".fc-datepickerView", {
            // Associate the text input to a DD/MM/YYYY date format
            // Show the week numbers
            showWeeks: false,
            // Set a statusbar format
            //statusFormat: "%d%S %F %Y",
            // Remove the "Today" button
            noTodayButton: true,
            // Fill the entire grid with dates
            fillGrid: false,
            // Enable the selection of dates not within the current month
            // but rendered within the grid (as we used fillGrid:true)
            constrainSelection: false,
            nopopup: true,
            //Dias con eventos
            callbackFunctions :{
                //"redraw":[function () {
                //    var listDate = ["20151210","20151211","20151216","20151230","20151231"];
                //    var optionStyle = {
                //        "backgroundColor":"#FF7A4D",
                //        "color": "#fff"
                //    };
                //    $(".date-picker-table tr td").attr("style","");
                //    //console.log('hola');
                //    for (var i = 0; i < listDate.length; i++) {
                //        //console.log($(".cd-" + listDate[i]).attr("class"));
                //        $(".cd-" + listDate[i]).css(optionStyle);
                //    }
                //}]
            }

            //para restringir todos los dias anteriores a hoy por ejemplo: rangeLow:new Date()
            // Set a range low of 13/03/1970 using a YYYYMMDD format String (Example): rangeLow:"19700313"
        },
        {
            //opts Opciones del datepicker
            "nodrag": 1
            //"lang": "en" // "lang":"en-US" (requiere libreria en carpeta lang)
        }
    );
// Inicializacion TimePicker
    dFrontJs.initTimePicker('.fc-timepicker_init', {
        'timeFormat': 'H:i',
        'className': 'mod_timepicker',
        'minTime': '9:00',
        'maxTime': '19:30',
        'disableTimeRanges': [
            ['10:00', '10:30'],
            ['16:00', '17:30']
        ]
    });

    dFrontJs.initControlInputFile('.fc-inputFile');
    dFrontJs.initEraserTags('.fc-eraserTag');
    // dFrontJs.initMachineReset('.fc-machine_play');
});

$(window).resize(function () {
    dFrontJs.colocarBtnSliderFade(".mod-sliderFade .slf-content .slf-groupParraf");
    dFrontJs.colocarBtnSliderCenter(".mod-sliderCenter");
})